"use client";

import { useEffect, useState } from "react";
import Image from "next/image";
import JSONPretty from "react-json-pretty";

export default function Home() {
  const [result, setResult] = useState<any>({});
  const [showResult, setShowResult] = useState<boolean>(false);

  useEffect(() => {
    initScore();
  }, []);

  // function to create initial object format required by the lecturer like this:
  // {
  //   aspek_penilaian_1: {
  //     mahasiswa_1: 0,
  //     ...
  //     mahasiswa_10: 0,
  //   },
  //   ...
  //   aspek_penilaian_10: {
  //     mahasiswa_1: 0,
  //     ...
  //     mahasiswa_10: 0,
  //   },
  // }
  const initScore = () => {
    let scores: any = {};
    for (let i = 1; i <= 4; i++) {
      const aspekPenilaian = `aspek_penilaian_${i}`;
      scores[aspekPenilaian] = {};
      for (let j = 1; j <= 10; j++) {
        const mahasiswa = `mahasiswa_${j}`;
        scores[aspekPenilaian][mahasiswa] = 0;
      }
    }

    setResult(scores);
  };

  const scrollToResult = () => {
    const element = document.getElementById("result");
    element?.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <main className="flex min-h-screen flex-col items-center px-24 py-8">
      <div className="z-10 w-full max-w-6xl items-center justify-between font-mono text-sm lg:flex mb-10">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          Aplikasi Penilaian Mahasiswa
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
          <a
            className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
            href="https://drive.google.com/file/d/11r0Yy12tz7b4rLHTUMT6l4R1vrt9hK4u/view?usp=share_link"
            target="_blank"
            rel="noopener noreferrer"
          >
            By Farhan Fahrezi
          </a>
        </div>
      </div>

      <div className="grid w-full max-w-6xl">
        <form>
          <div className="grid grid-cols-5 gap-4 items-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30 mb-4">
            {Array.from(Array(5).keys()).map((index) => (
              <div key={"score-header-" + index}>
                {index > 0 && (
                  <div className="text-center">Aspek Penilaian {index}</div>
                )}
              </div>
            ))}
          </div>

          {Array.from(Array(10).keys()).map((mahasiswaIndex) => (
            <div
              key={"mahasiswa-" + mahasiswaIndex}
              className="grid grid-cols-5 gap-4 items-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30 mb-4"
            >
              <div className="flex flex-row items-center">
                <Image
                  src={`/profile-pic-${mahasiswaIndex + 1}.png`}
                  alt="Vercel Logo"
                  className="mr-4"
                  width={32}
                  height={32}
                  priority
                />
                <span>Mahasiswa {mahasiswaIndex + 1}</span>
              </div>
              {Object.keys(result).map((aspekPenilaian, aspekIndex) => (
                <div key={"aspek-penilaian-" + aspekIndex}>
                  <select
                    className="border text-gray-900 font-medium text-base rounded-lg border-transparent bg-white/90 focus:border-transparent focus:ring-0 block w-full p-2.5 pr-5"
                    onChange={(e) => {
                      const newResult = { ...result };
                      newResult[aspekPenilaian][
                        `mahasiswa_${mahasiswaIndex + 1}`
                      ] = Number(e.target.value);
                      setResult(newResult);
                    }}
                  >
                    {Array.from(Array(11).keys()).map((index) => (
                      <option key={"option-" + index} value={index}>
                        {index}
                      </option>
                    ))}
                  </select>
                </div>
              ))}
            </div>
          ))}

          <div className="flex items-center justify-end mt-4">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="button"
              data-modal-target="defaultModal"
              data-modal-toggle="defaultModal"
              onClick={() => {
                setShowResult(true);
                setTimeout(() => {
                  scrollToResult();
                }, 100);
              }}
            >
              Simpan
            </button>
          </div>
        </form>
      </div>

      {showResult && (
        <div
          id="result"
          className="z-10 w-full max-w-6xl font-mono text-sm my-10 scroll-m-4"
        >
          <div className="left-0 top-0 flex flex-1 flex-col w-full border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
            Result :
            <br />
            <br />
            <JSONPretty id="json-pretty" data={result}></JSONPretty>
          </div>
        </div>
      )}
    </main>
  );
}
